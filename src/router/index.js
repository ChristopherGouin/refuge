import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path : '/',
    name : 'pets',
    component: () => import('../views/PetList.vue')
  },{
    path : '/ajout-resident',
    name : 'addpets',
    component: () => import('../views/AddPet.vue')
  },{
    path : '/modification-resident',
    name : 'editpet',
    props: (route) => ({
      ...route.params
    }),
    component: () => import('../views/EditPet.vue')
  },{
    path : '/vue-resident',
    name : 'vuepet',
    props: (route) => ({
      editDisable : true,
      ...route.params,
    }),
    component: () => import('../views/EditPet.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
