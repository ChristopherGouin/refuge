import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pets :  [
      { 
        name: 'Tom', spacies: 'Chat', gender: 'male', age: 10, situation: "abandoned", 
        cares: [
          {date: "21/04/2020", type: "cachet"},
          {date: "21/04/2020", type: "piqure"},
          {date: "21/04/2020", type: "autre"}
        ]
      },
      { name: 'Jerry', spacies: 'Sourie', gender: 'femelle', age: 15, situation: "abandoned", cares: []},
      { name: 'Jack', spacies: 'Moineaux', gender: 'male', age: 5, situation: "abandoned", cares: []},
      { name: 'Sparrow', spacies: 'Chat', gender: 'femelle', age: 5, situation: "abandoned", cares: []},
    ],
    currentPet: 0,
    adoptedPet: []
  },
  getters:{
    adoptedPetCount: function (state) {
      return state.adoptedPet.length
    }
  },
  mutations: {
    setCurrentPet (state, p){
      state.currentPet = state.pets.indexOf(p)
    },
    addPet (state, p){
      state.pets.push(p)
    },
    deletePet (state, p){
      const i = state.pets.indexOf(p);
      if (i > -1) {
        state.pets.splice(i, 1)
      }
    },
    updatePet (state, p){
      state.pets[state.currentPet] = p
    },
    addCare (state, c){
      state.pets[state.currentPet].cares.push(c)
    },
    addAdoptedPet (state, p){
      state.pets[state.pets.indexOf(p)].situation = "adopted"
      state.adoptedPet.push(state.pets[state.pets.indexOf(p)])
    },
    removeAdoptedPet (state, p){
      state.pets[state.pets.indexOf(p)].situation = "abandoned"
      state.adoptedPet.splice(state.adoptedPet.indexOf(p), 1)
    } 
  },
  actions: {
  },
  modules: {
  }
})
