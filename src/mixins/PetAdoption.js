export default {
	computed:{
		isAdopted: function () {
			return this.$store.state.adoptedPet.indexOf(this.pet) > -1
		}
	},
	methods:{
		adoptPet: function () {
			this.$store.commit('addAdoptedPet', this.pet)
		},
		DontAdoptPet: function () {
			this.$store.commit('removeAdoptedPet', this.pet)
		}
	}
}